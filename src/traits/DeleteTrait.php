<?php


namespace ffhome\frame\traits;


use ffhome\frame\service\SystemDeleteService;
use think\facade\Db;

trait DeleteTrait
{
    /**
     * 删除时记录到日志表中
     * @var bool
     */
    protected $recordDelete = true;

    /**
     * 删除操作
     * @param array|int $id 删除的主键
     */
    public function delete($id)
    {
        Db::transaction(function () use ($id) {
            $row = $this->onBeforeDelete($id);
            if ($this->deleteField === false) {
                Db::name($this->modelName)->delete($id);
            } else {
                Db::name($this->modelName)->whereIn('id', $id)->update([$this->deleteField => date('Y-m-d H:i:s')]);
            }
            $this->onAfterDelete($id, $row);
        });
        $this->success($this->getDeleteSuccessInfo($id));
    }

    /**
     * 删除成功的信息
     * @param $id
     * @return string
     */
    protected function getDeleteSuccessInfo($id): string
    {
        return lang('common.delete_success');
    }

    /**
     * 删除操作前触发的事件
     * @param array|int $id 删除的主键
     * @return array 将要删除的数据库数据（可选）
     */
    protected function onBeforeDelete($id): array
    {
        $row = [];
        if ($this->recordDelete) {
            $row = Db::name($this->modelName)->whereIn('id', $id)->select()->toArray();
            (new SystemDeleteService())->saveRecords($row, $this->modelName, $this->getCurrentUserId());
        }
        return $row;
    }

    /**
     * 删除操作后触发的事件
     * @param array|int $id 删除的主键
     * @param array $row 将要删除的数据库数据（可选）
     */
    protected function onAfterDelete($id, array $row)
    {
        $this->clearCache();
    }
}