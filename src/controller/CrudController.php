<?php
declare (strict_types=1);

namespace ffhome\frame\controller;

use ffhome\frame\traits\DeleteTrait;
use ffhome\frame\traits\SaveTrait;

abstract class CrudController extends QueryController
{
    use SaveTrait;
    use DeleteTrait;
}