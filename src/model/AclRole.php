<?php
declare (strict_types=1);

namespace ffhome\frame\model;

class AclRole extends BaseModel
{
    const MODEL_NAME = 'acl_role';

    /**
     * 全部数据权限
     */
    const DS_ALL = 1;

    /**
     * 自定数据权限
     */
    const DS_CUSTOM = 2;

    /**
     * 部门数据权限
     */
    const DS_DEPT = 3;

    /**
     * 部门及以下数据权限
     */
    const DS_DEPT_AND_CHILD = 4;

    /**
     * 仅本人数据权限
     */
    const DS_SELF = 5;
}